import * as types from '../constant/actionTypes'
import axios from 'axios';
import { BASE_URL } from '../constant/general';

export const getDetailProductsRequest = () => ({
  type: types.GET_PRODUCTS_REQUEST,
});

export const getDetailProductsSuccess = (detail) => ({
  type: types.GET_DETAIL_SUCCESS,
  payload: detail,
});

export const getDetailProductsFailure = (error) => ({
  type: types.GET_DETAIL_FAILURE,
  error,
});

export const getDetailProductsAction = (id) => {
  return async (dispatch) => {
    try {
      dispatch(getDetailProductsRequest());
      const res = await axios.get(`${BASE_URL}/home/${id}`)
      dispatch(getDetailProductsSuccess(res.data.data));
      console.log("detail action", res)
    } catch (error) {
      // dispatch(getDetailProductsFailure(error));
    }
  }
}