import Axios from 'axios'
import * as types from '../constant/actionTypes'
import { BASE_URL, TOKEN } from '../constant/general'

export const getDetailProductSellerRequest = ()=> ({
    type: types.GET_DETAIL_PRODUCT_SELER_REQUEST,
})
export const getDetailProductSellerSuccess = (detail)=> ({
    type: types.GET_DETAIL_PRODUCT_SELER_SUCCESS,
    payload: detail
})
export const getDetailProductSellerFailure = (error)=> ({
    type: types.GET_DETAIL_PRODUCT_SELER_FAILURE,
    error: error,
})

export const getDetailProductSellerAction = (param)=>{
    return async (dispatch) => {
        dispatch(getDetailProductSellerRequest())
        Axios.get(`${BASE_URL}/home/${param}`,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(getDetailProductSellerSuccess(res?.data))
            console.log("Success Getdetail",res?.data)
        })
        .catch((error)=>{
            dispatch(getDetailProductSellerFailure(error))
            console.log("Gagagl Getdetail",error)
        })
    }
}