import * as types from '../constant/actionTypes'
import axios from 'axios'
import { TOKEN, BASE_URL } from '../constant/general'
import { getReviewAction } from './getReviewAction'

export const storeReviewRequest = ()=> ({
    type: types.STORE_REVIEW_REQUEST
})

export const storeReviewSuccess = (detail)=> ({
    type: types.STORE_REVIEW_SUCCESS,
    payload: detail,
})

export const storeReviewFailure = (error)=> ({
    type: types.STORE_REVIEW_FAILURE,
    error,
})


export const storeReviewAction = (data)=>{
    return async (dispatch)=>{
        
        dispatch(storeReviewRequest())
        axios.post(`${BASE_URL}/review/create/${data?.product_id}`,data?.data,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(storeReviewSuccess(res?.data))
            dispatch(getReviewAction(data?.product_id))
            console.log("Store Reviews",res?.data)
        })
        .catch((error)=>{
            dispatch(storeReviewFailure(error))
            console.log("Store Reviews Failure",error)
        })
    }
}
