import * as Types from '../constant/actionTypes'
import { BASE_URL,TOKEN } from '../constant/general'
import axios from 'axios'
import { getCartAction } from './getCartAction'

export const createTransactionRequest = ()=>({
    type: Types.CREATE_TRANSACTION_REQUEST,
})

export const createTransactionSuccess = (detail)=>({
    type: Types.CREATE_TRANSACTION_SUCCESS,
    payload: detail
})

export const createTransactionFailure = (error)=>({
    type: Types.CREATE_TRANSACTION_FAILURE,
    error: error
})


export const CreateTransactionAction = (data)=>{
    const posed = {
        "productId" : [`${data?.productId}`],
        "quantity" : [data?.quantity]
    }
    console.log(posed)

    return async (dispatch) =>{
        dispatch(createTransactionRequest())
        axios.post(`${BASE_URL}/transaction/create`,posed,{
            headers: {
                'Authorization': `bearer ${TOKEN}`
            }
        })
        .then((res)=>{
            dispatch(createTransactionSuccess(res?.data))
            dispatch(getCartAction())
            console.log("Succes ?",res?.data)
        })
        .catch((error)=>{
            dispatch(createTransactionFailure(error))
            console.log("Gagal ?",error)
        })
    }
}