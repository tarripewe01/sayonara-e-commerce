import * as types from '../constant/actionTypes'
import axios from 'axios'
import { BASE_URL, TOKEN } from '../constant/general'
import { getProfileAction } from './getProfileAction'

export const updateProfileRequest = ()=> ({
    type: types.UPDATE_PROFILE_REQUEST,
})

export const updateProfileSuccess = (detail)=>({
    type: types.UPDATE_PROFILE_SUCCESS,
    payload: detail,
})
export const updateProfileFailure = (error)=>({
    type: types.UPDATE_REVIEW_FAILURE,
    error,
})

export const updateProfileAction = (data)=>{
    console.log(data)
    return async (dispatch)=>{
        dispatch(updateProfileRequest());
        axios.put(`${BASE_URL}/profile/update/`,data,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res) => {
            dispatch(updateProfileSuccess(res?.data))
            dispatch(getProfileAction())
            console.log("success update PROFILE",res?.data)
        })
        .catch((error)=>{
            dispatch(updateProfileFailure(error))
            console.log("Ggagal update PROFILE",error)
        })
    }
}