import Axios from 'axios'
import * as types from '../constant/actionTypes'
import { BASE_URL, TOKEN } from '../constant/general'
import { getDiscusionAction } from './getDiscusionAction'

export const putDiscusionRequest = ()=> ({
    type: types.PUT_DISCUSION_REQUEST,
})
export const putDiscusionSuccess = (detail)=> ({
    type: types.PUT_DISCUSION_SUCCESS,
    payload: detail
})
export const putDiscusionFailure = (error)=> ({
    type: types.PUT_DISCUSION_FAILURE,
    error: error,
})

export const putDiscusionAction = (data)=>{
    console.log("===DATA===",data)
    return async (dispatch) => {
        dispatch(putDiscusionRequest())
        Axios.put(`${BASE_URL}/discuss/${data?.idComent}/comment`,
        data?.data,
        {
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(putDiscusionSuccess(res?.data))
            dispatch(getDiscusionAction(data?.param))
            console.log("Success balas Discusion",res?.data)
        })
        .catch((error)=>{
            dispatch(putDiscusionFailure(error))
            console.log("Gagagl balas Discusion",error)
        })
    }
}