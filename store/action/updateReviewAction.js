import * as types from '../constant/actionTypes'
import axios from 'axios'
import { BASE_URL, TOKEN } from '../constant/general'
import { getReviewAction } from './getReviewAction'

export const updateReviewRequest = ()=> ({
    type: types.GET_REVIEW_REQUEST,
})

export const updateReviewSuccess = (detail)=>({
    type: types.GET_REVIEW_SUCCESS,
    payload: detail,
})
export const updateReviewFailure = (error)=>({
    type: types.GET_REVIEW_FAILURE,
    error,
})

export const updateReviewAction = (data)=>{
    console.log(data)
    return async (dispatch)=>{
        dispatch(updateReviewRequest());
        axios.put(`${BASE_URL}/review/update/${data?.idproduct}/${data?.idbuyer}`,data?.data,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res) => {
            dispatch(updateReviewSuccess(res?.data))
            console.log("success update review",res?.data)
            dispatch(getReviewAction(data?.idproduct))
        })
        .catch((error)=>{
            dispatch(updateReviewFailure(error))
            console.log("Ggagal update review",error)
        })
    }
}