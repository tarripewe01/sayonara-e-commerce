import Axios from 'axios'
import * as types from '../constant/actionTypes'
import { BASE_URL, TOKEN } from '../constant/general'

export const getCartRequest = ()=> ({
    type: types.GET_CART_REQUEST,
})
export const getCartSuccess = (detail)=> ({
    type: types.GET_CART_SUCCESS,
    payload: detail
})
export const getCartFailure = (error)=> ({
    type: types.GET_CART_FAILURE,
    error: error,
})

export const getCartAction = ()=>{
    return async (dispatch) => {
        dispatch(getCartRequest())
        Axios.get(`${BASE_URL}/cart`,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(getCartSuccess(res?.data))
            console.log("Success GetCart",res?.data)
        })
        .catch((error)=>{
            dispatch(getCartFailure(error))
            console.log("Gagagl GetCart",error)
        })
    }
}