import * as types from '../constant/actionTypes';
import axios from 'axios';
import { BASE_URL, TOKEN } from '../constant/general';

export const historyBuyerRequest = () => ({
  type: types.GET_HISTORY_BUYER_REQUEST
});

export const historyBuyerSuccess = (payload) => ({
  type: types.GET_HISTORY_BUYER_SUCCESS,
  payload
});

export const historyBuyerFailure = (error) => ({
  type: types.GET_HISTORY_BUYER_FAILURE,
  error
});

export const historyBuyerAction = ( payload ) => {
  return async (dispatch) => {
    try {
      dispatch( historyBuyerRequest() );
      const res = await axios.get( `${BASE_URL}/transaction/buyer`, {
        headers: { Authorization: `Bearer ${TOKEN}`},
      });
      console.log('ini res oi', res.data.data)
      dispatch( historyBuyerSuccess(res.data.data) )
    } catch (error) {
      console.log('Error UserProfile', error);
      dispatch(historyBuyerFailure(error));
    }
  };
};