import * as types from '../constant/actionTypes'
import axios from 'axios'
import { BASE_URL, TOKEN } from '../constant/general'

export const getProfileRequest = ()=> ({
    type: types.GET_PROFILE_REQUEST,
})

export const getProfileSuccess = (detail)=>({
    type: types.GET_PROFILE_SUCCESS,
    payload: detail,
})
export const getProfileFailure = (error)=>({
    type: types.GET_PROFILE_FAILURE,
    error,
})

export const getProfileAction = ()=>{
    return async (dispatch)=>{
        dispatch(getProfileRequest());
        axios.get(`${BASE_URL}/profile`,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res) => {
            dispatch(getProfileSuccess(res?.data))
            console.log("success get PROFILE",res?.data)
        })
        .catch((error)=>{
            dispatch(getProfileFailure(error))
            console.log("Ggagal get PROFILE",error)
        })
    }
}