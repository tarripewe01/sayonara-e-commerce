import * as types from '../constant/actionTypes'
import axios from 'axios';
import { BASE_URL } from '../constant/general';

export const getProductsRequest = () => ({
  type: types.GET_PRODUCTS_REQUEST,
});

export const getProductsSuccess = (product) => ({
  type: types.GET_PRODUCTS_SUCCESS,
  payload: product,
});

export const getProductsFailure = (error) => ({
  type: types.GET_PRODUCTS_FAILURE,
  error,
});

export const getProductsAction = () => {
  return async (dispatch) => {
    try {
      dispatch(getProductsRequest());
      const res = await axios.get(`${BASE_URL}/home`)
      dispatch(getProductsSuccess(res.data.data));
    } catch (error) {
      dispatch(getProductsFailure(error));
    }
  }
}