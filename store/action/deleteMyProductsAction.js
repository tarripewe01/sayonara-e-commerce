import * as types from '../constant/actionTypes'
import axios from 'axios';
import { BASE_URL, TOKEN } from '../constant/general';
import { myProductsAction } from './getMyProductsAction';

export const deleteMyProductsRequest = () => ({
  type: types.DELETE_MYPRODUCTS_REQUEST,
});

export const deleteMyProductsSuccess = (item) => ({
  type: types.DELETE_MYPRODUCTS_SUCCESS,
  payload: item,
});

export const deleteMyProductsFailure = (error) => ({
  type: types.DELETE_MYPRODUCTS_FAILURE,
  error,
});

export const deleteMyProductsAction = (data) => {
  console.log(data?.fullname)
  return async (dispatch) => {
    axios.delete(`${BASE_URL}/home/delete/${data?.myid}`,{
    headers: {'Authorization': `bearer ${TOKEN}`}
    })
    .then(res=>{
      dispatch(deleteMyProductsSuccess(res.data));
      dispatch(myProductsAction(data?.fullname));
      console.log("DELETE MYSUCCESS",res?.data)
    })
    .catch(error=>{
      dispatch(deleteMyProductsFailure(error));
      console.log("Error Delete My Products",error)
    })

  }
}