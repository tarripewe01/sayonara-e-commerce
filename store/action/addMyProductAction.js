import Axios from 'axios'
import * as types from '../constant/actionTypes'
import { BASE_URL, TOKEN } from '../constant/general'
import { myProductsAction } from './getMyProductsAction'

export const addMyProductRequest = ()=> ({
    type: types.ADD_MYPRODUCT_REQUEST,
})
export const addMyProductSuccess = (detail)=> ({
    type: types.ADD_MYPRODUCT_SUCCESS,
    payload: detail
})
export const addMyProductFailure = (error)=> ({
    type: types.ADD_MYPRODUCT_FAILURE,
    error: error,
})

export const addMyProductAction = (data)=>{
    console.log(data?.dataToSend)
    return async (dispatch) => {
        dispatch(addMyProductRequest())
        Axios.post(`${BASE_URL}/home/create`,data?.dataToSend,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(addMyProductSuccess(res?.data))
            dispatch(myProductsAction(data?.seller))
            console.log("Success store product",res?.data)
        })
        .catch((error)=>{
            dispatch(addMyProductFailure(error))
            console.log("Gagagl store product",error)
        })
    }
}