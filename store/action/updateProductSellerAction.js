import axios from 'axios'
import * as types from '../constant/actionTypes'
import { BASE_URL, TOKEN } from '../constant/general'
import { myProductsAction } from './getMyProductsAction'

export const updateProductSellerRequest = ()=> ({
    type: types.UPDATE_PRODUCT_SELER_REQUEST,
})

export const updateProductSellerSuccess = (detail)=>({
    type: types.UPDATE_PRODUCT_SELER_SUCCESS,
    payload: detail,
})

export const updateProductSellerFailure = (error)=>({
    type: types.UPDATE_PRODUCT_SELER_FAILURE,
    error,
})

export const updateProductSellerAction = (data)=>{
    console.log("===MYDATA===",data)
    return async (dispatch)=>{
        dispatch(updateProductSellerRequest());
        axios.put(`${BASE_URL}/home/update/${data?.idProduct}`,data?.data,{
            headers: {
                'Authorization': `bearer ${TOKEN}`,
                'Content-Type': 'multipart/form-data; ',
            },
        })
        .then((res) => {
            dispatch(updateProductSellerSuccess(res?.data))
            dispatch(myProductsAction(data?.seller))
            console.log("success updatemyproduct",res?.data)
        })
        .catch((error)=>{
            dispatch(updateProductSellerFailure(error))
            console.log("Ggagal updatemyproduct",error)
        })
    }
}