import * as Types from '../constant/actionTypes'

const initialState = {
    loading: false,
    data: [],
    error: null
}


function postDiscusionReducer(state = initialState,action){
    switch(action.type){
        case Types.POST_DISCUSION_REQUEST:
            return {
                loading: true,
                data: [],
                error: null
            }
        case Types.POST_DISCUSION_SUCCESS:
            return {
                loading: false,
                data: action.payload,
                error: null
            }
        case Types.POST_DISCUSION_FAILURE:
            return {
                loading: false,
                data: [],
                error: action.error
            }
        default:
            return { ...state }
    }
}

export default postDiscusionReducer