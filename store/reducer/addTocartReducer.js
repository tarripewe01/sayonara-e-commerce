import * as types from '../constant/actionTypes'

const initalState = {
    loading : false,
    data : null,
    error: false
}

function addTocartReducer(state = initalState, action){
    switch(action.type){
        case types.ADD_TO_CART_REQUEST : 
        return Object.assign({},state,{
            loading: true,
        })
        case types.ADD_TO_CART_SUCCESS:
            return Object.assign({}, state,{
                loading: false,
                data: action.payload,
                error: false
            })
        case types.ADD_TO_CART_FAILURE:
            return Object.assign({}, state,{
                loading: false,
                error : action.error
            })
        default:
            return { ...state }
    }
}

export default addTocartReducer;