import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  profile: [],
  error: null,     
};

function updateProfileReducer(state = initialState, action) {
  switch (action.type) {
    case types.UPDATE_PROFILE_REQUEST:
      return {
        loading: true,
        profile: null,
        error: null
      }
    case types.UPDATE_PROFILE_SUCCESS:
      return {
        loading: false,
        profile: action.payload,  
        error: null,
      }
    case types.UPDATE_PROFILE_FAILURE:
      return {
        loading: false,
        error: action.error
      }
    default:
      return { ...state }
  }
}

export default updateProfileReducer;