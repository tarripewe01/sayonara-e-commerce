import {combineReducers} from 'redux';
import productReducer from './productReducer';
import cartItemsReducer from './cartItemReducer';
import detailProductReducer from './detailProductReducer';
import getReviewReducer from './getReviewReducer';
import storeReviewsReducer from './storeReviewReducer';
import addTocartReducer from './addTocartReducer';
import getCartReducer from './getCartReducer'
import destroyCartReducer from './destroyCardReducer'
import createTransactionReducer from './CreateTransactionReducer'
import updateReviewReducer from './updateReviewReducer'
import getDiscusionReducer from './getDiscusionReducer'
import postDiscusionReducer from './postDiscusionReducer'
import putDiscusionReducer from './putDiscusionReducer'
import userProfileReducer from './userProfileReducer'
import historyBuyerReducer from './historyBuyerReducer'
import myProductsReducer from './getMyProductsReducer'
import notificationSellerReducer from './notificationSellerReducer';
import deleteMyProductsReducer from './deleteMyProductsReducer';
import destroyProduct from './destroyProduct';
import getDetailProductSellerReducer from './getDeatilProductSelerReducer'
import updateProductSellerAction from './updateProductSellerReducer'
import addMyProductReducer from './addMyProductReducer'
import storeToCartReducer from './storeToCartReducer'
import updateProfileReducer from './updateProfileReducer'
import getProfileReducer from './getProfileReducer'

const rootReducer = combineReducers({
  productReducer: productReducer,
  cartItemsReducer: cartItemsReducer,
  detailProductReducer: detailProductReducer,
  getReviewReducer: getReviewReducer,
  storeReviewsReducer: storeReviewsReducer,
  addTocartReducer: addTocartReducer,
  getCartReducer: getCartReducer,
  destroyCartReducer: destroyCartReducer,
  createTransactionReducer: createTransactionReducer,
  updateReviewReducer: updateReviewReducer,
  getDiscusionReducer: getDiscusionReducer,
  postDiscusionReducer: postDiscusionReducer,
  putDiscusionReducer: putDiscusionReducer,
  userProfileReducer: userProfileReducer,
  historyBuyerReducer: historyBuyerReducer,
  myProductsReducer: myProductsReducer,
  notificationSellerReducer: notificationSellerReducer,
  deleteMyProductsReducer: deleteMyProductsReducer,
  destroyProduct: destroyProduct,
  getDetailProductSellerReducer:getDetailProductSellerReducer,
  updateProductSellerAction: updateProductSellerAction,
  addMyProductReducer: addMyProductReducer,
  storeToCartReducer: storeToCartReducer,
  updateProfileReducer: updateProfileReducer,
  getProfileReducer: getProfileReducer, 
});

export default rootReducer;