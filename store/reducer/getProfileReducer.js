import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  profile: [],
  error: null,     
};

function getProfileReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_PROFILE_REQUEST:
      return {
        loading: true,
        profile: null,
        error: null
      }
    case types.GET_PROFILE_SUCCESS:
      return {
        loading: false,
        profile: action.payload,  
        error: null,
      }
    case types.GET_PROFILE_FAILURE:
      return {
        loading: false,
        error: action.error
      }
    default:
      return { ...state }
  }
}

export default getProfileReducer;