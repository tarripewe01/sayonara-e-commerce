import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  reviews: [],
  error: null,     
};

function getReviewReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_REVIEW_REQUEST:
      return {
        loading: true,
        reviews: null,
        error: null
      }
    case types.GET_REVIEW_SUCCESS:
      return {
        loading: false,
        reviews: action.payload,  
        error: null,
      }
    case types.GET_REVIEW_FAILURE:
      return {
        loading: false,
        error: action.error
      }
    default:
      return { ...state }
  }
}

export default getReviewReducer;