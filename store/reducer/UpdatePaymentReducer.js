import * as types from '../constant/actionTypes'

const initialState = {
    loading: false,
    data: [],
    error: false
}

function updatePaymentAction(state = initialState, action){
    switch(action.type){
        case types.UPDATE_PAYMENT_REQUEST:
            return {
                loading: true,
                data: [],
                error: null,
            }
        case types.UPDATE_PAYMENT_SUCCESS:
            return {
                loading: false,
                data: action.payload,
                error: null
            }
        case types.UPDATE_REVIEW_FAILURE:
            return {
                loading: false,
                data: [],
                error: action.error
            }
        default:
        return { ...state };
    }
}

export default updatePaymentAction;