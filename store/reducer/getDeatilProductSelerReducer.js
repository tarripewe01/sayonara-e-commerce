import * as types from '../constant/actionTypes'

const initialState = {
    loading: false,
    data: [],
    error: null
}

const getDetailProductSellerReducer = (state = initialState, action) => {
    switch(action.type){
        case types.GET_DETAIL_PRODUCT_SELER_REQUEST:
            return {
                loading: true,
                data: null,
                error: null
            }
        case types.GET_DETAIL_PRODUCT_SELER_SUCCESS:
            return{
                loading: false,
                data: action.payload,
                error: null
            }
        case types.GET_DETAIL_PRODUCT_SELER_FAILURE:
            return{
                loading: false,
                data: null,
                error: action.error
            }
        default:
            return { ...state }
    }
}

export default getDetailProductSellerReducer;