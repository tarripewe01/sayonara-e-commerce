import * as types from '../constant/actionTypes'

const initialState = {
    loading: false,
    data: [],
    error: null
}

const addMyProductReducer = (state = initialState, action) => {
    switch(action.type){
        case types.ADD_MYPRODUCT_REQUEST:
            return {
                loading: true,
                data: null,
                error: null
            }
        case types.ADD_MYPRODUCT_SUCCESS:
            return{
                loading: false,
                data: action.payload,
                error: null
            }
        case types.ADD_MYPRODUCT_FAILURE:
            return{
                loading: false,
                data: null,
                error: action.error
            }
        default:
            return { ...state }
    }
}

export default addMyProductReducer;