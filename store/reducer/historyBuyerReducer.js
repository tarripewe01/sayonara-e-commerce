import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  data: {},
  error: null
};

function HistoryBuyerReducer (state= initialState, action) {
  switch ( action.type ){
    case types.GET_HISTORY_BUYER_REQUEST:
      return Object.assign({}, state, {
        loading: true
      });
    case types. GET_HISTORY_BUYER_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        data: action.payload
      });
    case types.GET_PRODUCTS_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      })
    default:
      return state;
  }
}

export default HistoryBuyerReducer;