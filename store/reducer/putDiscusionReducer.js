import * as Types from '../constant/actionTypes'

const initialState = {
    loading: false,
    data: [],
    error: null
}


function putDiscusionReducer(state = initialState,action){
    switch(action.type){
        case Types.PUT_DISCUSION_REQUEST:
            return {
                loading: true,
                data: [],
                error: null
            }
        case Types.PUT_DISCUSION_SUCCESS:
            return {
                loading: false,
                data: action.payload,
                error: null
            }
        case Types.PUT_DISCUSION_FAILURE:
            return {
                loading: false,
                data: [],
                error: action.error
            }
        default:
            return { ...state }
    }
}

export default putDiscusionReducer