import * as types from '../constant/actionTypes'

const initialState = {
    loading: false,
    data: [],
    error: false
}

function updateProductSellerAction(state = initialState, action){
    switch(action.type){
        case types.UPDATE_PRODUCT_SELER_REQUEST:
            return {
                loading: true,
                data: [],
                error: null,
            }
        case types.UPDATE_PRODUCT_SELER_SUCCESS:
            return {
                loading: false,
                data: action.payload,
                error: null
            }
        case types.UPDATE_PRODUCT_SELER_FAILURE:
            return {
                loading: false,
                data: [],
                error: action.error
            }
        default:
        return { ...state };
    }
}

export default updateProductSellerAction;