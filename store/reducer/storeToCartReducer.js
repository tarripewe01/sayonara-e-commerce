import * as types from '../constant/actionTypes';

const initialState = {
  data: [],
}

const storeToCartReducer = ( state = initialState, action ) => {
  const { type, payload  } = action
  switch(type){
    case types.MORE:
      return {
        data:  [
            ...state.data,
            payload
        ],
      }
    // case types.LESS:
    //   return {
    //     data: payload
    //   }
    default:
      return { ...state }
  }
}   

export default storeToCartReducer;