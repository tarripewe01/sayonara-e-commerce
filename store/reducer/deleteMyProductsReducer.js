import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  product: [],
  error: null,     
};

function deleteMyProductsReducer(state = initialState, action) {
  switch (action.type) {
    case types.DELETE_MYPRODUCTS_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.DELETE_MYPRODUCTS_SUCCESS:
      return Object.assign({},state, {
        loading: false,
        product: action.payload,
        
      });
    case types.DELETE_MYPRODUCTS_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      });
    default:
      return state;
  }
}

export default deleteMyProductsReducer;