import * as types from '../constant/actionTypes'

const initialState = {
    loading : false,
    data: null,
    error: false,
}

function getDiscusionReducer(state = initialState,action){
    switch(action.type){
        case types.GET_DISCUSION_REQUEST:
            return {
                loading: true,
                data: null,
                error: null
            }
        case types.GET_DISCUSION_SUCCESS:
            return{
                loading: false,
                data: action.payload,
                error: null
            }
        case types.GET_DISCUSION_FAILURE:
            return{
                loading: false,
                data: null,
                error: action.error
            }
        default:
            return { ...state }
    }
}

export default getDiscusionReducer;