import createAxios from 'axios';

const axios = createAxios.create({
  baseURL: 'https://sayonara.kuyrek.com/'
})

export default axios