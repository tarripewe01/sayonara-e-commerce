const COLOR = {
  orange: '#ffa45b',
  yellow: '#ffda77',
  white: '#f7f6ed',
  pureWhite: '#fff',
  grey: '#e8e8e8',
  red: '#D65445',
  black: '#313131',
  blue: '#00b0bd',
  lightBlue: '#aee6e6',
  green: '#16a596',
  darkGreen: '#00917c'
}

export default COLOR;