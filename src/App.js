import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider } from 'react-redux';
import store from '../store';

import { 
  SplashScreen,LoginScreen, 
  RegisterScreen, SearchScreen,
  CreateProductScreen, EditProductScreen,
  DetailProductScreen, CartScreen,
  CheckoutScreen, ProfileScreen,
  EditProfileScreen, NotifScreen,
  Pembayaran
} from './screen';
import MainTabs from '../src/router';

const RootStack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootStack.Navigator initialRouteName='Home' screenOptions={{headerShown: false}}>
          <RootStack.Screen name='Splash' component={SplashScreen}/>
          <RootStack.Screen name='Login' component={LoginScreen}/>
          <RootStack.Screen name='Register' component={RegisterScreen}/>
          <RootStack.Screen name='Home' component={MainTabs} />
          <RootStack.Screen name='Search' component={SearchScreen}/>
          <RootStack.Screen name='Create Product' component={CreateProductScreen}/>
          <RootStack.Screen name='Edit Product' component={EditProductScreen}/>
          <RootStack.Screen name='Detail Product' component={DetailProductScreen}/>
          <RootStack.Screen name='Cart' component={CartScreen}/>
          <RootStack.Screen name='Checkout' component={CheckoutScreen}/>
          <RootStack.Screen name='Profile' component={ProfileScreen} />
          <RootStack.Screen name='Edit Profile' component={EditProfileScreen} />
          <RootStack.Screen name='Notification' component={NotifScreen} />
          <RootStack.Screen name='Pembayaran' component={Pembayaran} />
        </RootStack.Navigator>
      </NavigationContainer>
      </Provider>
  );
}

export default App;


