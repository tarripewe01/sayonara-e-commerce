import { call, put, takeLatest} from 'redux-saga/effects';

function* setData(action) {
  console.log('ini setData di sagas', action)
}

export default function* rootSaga() {
  yield takeLatest('SET_DATA_REQUESTED', setData)
}