import React, {useState,useEffect} from 'react'
import { 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Image,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';

import COLOR from '../../utils/Tools';
import * as ImagePicker from 'react-native-image-picker'
import Modal from 'react-native-modal';
import { useSelector, useDispatch } from 'react-redux';
import { icUp,icDown, DuBni, icCamera, icGallery } from '../../assets/Images'
import { updateProfileAction } from '../../../store/action/updateProfileAction';

const EditProfileScreen = ({navigation}) => {
    // const {colors} = useTheme();
    const dispatch = useDispatch();
    const [imageLaunch,setImageLaunch] = useState(false)
    const [isImage,setIsImage] = useState('')
    const [photo,setPhoto] = useState('')
    const [username,setUsername] = useState('')
    const [fullname,setFullname] = useState('')

    renderInner = () => (
      <View style={styles.panel}>
          <View style={{alignItems: 'center'}}>
            <Text style={styles.panelTitle}>Upload Photo</Text>
            <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
          </View>
          <TouchableOpacity style={styles.panelButton}>
            <Text style={styles.panelButtonTitle}>Take Photo</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.panelButton}>
            <Text style={styles.panelButtonTitle}>Choose From Library</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.panelButton} onPress={() => alert("a")}>
            <Text style={styles.panelButtonTitle}>Cancel</Text>
          </TouchableOpacity>
      </View>);

    const handleCamera = (type)=>{
      if(type == 'CAMERA'){
        ImagePicker.launchCamera({}, (response) => {
          console.log('Response = ', response);
        
         if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let url = {  uri: response?.uri }
            const source = { 
              uri: response.uri,
              type: response?.type,
              name: response?.fileName,
            };
            setIsImage(source)
            setPhoto(url)
          }
        });
      }else if(type == 'GALERY'){
        ImagePicker.launchImageLibrary({}, (response) => {
          console.log('Response = ', response);
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let url = {  uri: response?.uri }
            const source = { 
              uri: response.uri,
              type: response?.type,
              name: response?.fileName,
            };
            setIsImage(source)
            setPhoto(url)
          }
        });
      }
      setImageLaunch(false)
    }

    const handleUpdateProfile = () =>{
      const photoForUploaded  = new FormData()
      photoForUploaded.append('username', username)
      photoForUploaded.append('fullName', fullname)
      photoForUploaded.append('image', isImage)
      photoForUploaded.append('address', "jl.raya kemang no 10")
      dispatch(updateProfileAction(photoForUploaded))
      navigation.navigate('Profile')
    }

    const AddImage = ()=>(
      <Modal
        isVisible={imageLaunch}
        onBackButtonPress={() => setImageLaunch(false)}
        onBackdropPress={() => setImageLaunch(false)}
        backdropTransitionOutTiming={100}
        style={{margin: 0, justifyContent: 'flex-end'}}
      >
        <View style={{backgroundColor: 'white', paddingHorizontal: 10, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, justifyContent: 'space-between'}}>
          <View style={{alignContent: 'center', justifyContent: 'center',alignItems: 'center'}}>
            <TouchableOpacity 
            onPress={() => setImageLaunch(false)}
            activeOpacity={0.9}
            style={{width: 80, height: 5, marginTop: 5, borderRadius: 15,marginBottom: 5,backgroundColor:   "#DDDDDD"}}>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPressIn={()=>handleCamera('CAMERA')} style={{flexDirection: 'row',alignItems: 'center',marginTop: 15, marginBottom: 2}}>
            <Image source={icCamera} style={{width: 24, height: 24, marginRight: 8}}/>
            <Text>Take Picture</Text>
          </TouchableOpacity>
          <TouchableOpacity onPressIn={()=>handleCamera('GALERY')} style={{flexDirection: 'row',alignItems: 'center',marginBottom: 15, marginTop: 2}}>
            <Image source={icGallery} style={{width: 24, height: 24, marginRight: 8}}/>
            <Text>Gallery</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    )

  return (
    <View style={styles.container}>
        {AddImage()}
        <View style={{alignItems:'center', marginTop: 100}}>
        </View>
        <TouchableOpacity onPress={() => setImageLaunch(true)}>
          <View style={{alignItems: 'center',justifyContent: 'center'}}>
            { isImage ? (
              <Image source={photo && photo } style={{borderRadius: 100, width: 100, height: 100,backgroundColor: 'gray'}} resizeMode="cover"/>
              ):(
                <Image source={{uri: "https://picsum.photos/200/300"}} style={{borderRadius: 100, width: 100, height: 100,backgroundColor: 'gray'}}/>
              )
            }
          </View>   
        </TouchableOpacity>
        <View style={styles.action}>
          <FontAwesome name="user-o" size={20} />
          <TextInput
          placeholder="Username" 
          placeholderTextColor={COLOR.black}
          autoCorrect={false}
          value={username}
          onChangeText={(value) => setUsername(value)}
          style={styles.textInput}
          />
        </View>
        <View style={styles.action}>
          <FontAwesome name="user-o" size={20} />
          <TextInput
          placeholder="Full Name" 
          placeholderTextColor={COLOR.black}
          keyboardType='email-address'
          value={fullname}
          onChangeText={(value) => setFullname(value)}
          autoCorrect={false}
          style={styles.textInput}
          />
        </View>
        <TouchableOpacity style={styles.commandButton} onPress={handleUpdateProfile}>
            <Text style={styles.panelButtonTitle}>Save</Text>
        </TouchableOpacity>
    </View>
  )
}

export default EditProfileScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,padding: 10
  },
  commandButton: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: COLOR.green,
    alignItems: 'center',
    marginTop: 10,
  },
  panel: {
    padding: 20,
    backgroundColor: COLOR.white,
    paddingTop: 20,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    // shadowColor: '#000000',
    // shadowOffset: {width: 0, height: 0},
    // shadowRadius: 5,
    // shadowOpacity: 0.4,
  },
  header: {
    backgroundColor: COLOR.white,
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: COLOR.black,
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: COLOR.green,
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: COLOR.green,
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
});
