import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';

import { 
  SplashScreen, LoginScreen, 
  RegisterScreen, HomeScreen,
  SearchScreen, HomeStack
} from '..';

const Stack = createStackNavigator();

const AuthScreen = () => {
  return (
      <Stack.Navigator >
        <Stack.Screen name="Splash" component={SplashScreen} 
          options={{headerShown: false}}/>
        <Stack.Screen name="Login" component={LoginScreen} 
          options={{headerShown: false}}/>
        <Stack.Screen name="Register" component={RegisterScreen} 
          options={{headerShown: false}}/>
        <Stack.Screen name="Home" component={HomeStack} 
          options={{headerShown: false}}/>
      </Stack.Navigator>
  )
}

export default AuthScreen;

// const HomeStack= () => {
//   return(
//     <HomeStack.Navigator>
//       <HomeStack.Screen name="Home" component={HomeScreen} />
//       <HomeStack.Screen name="Search" component={SearchScreen} />
//     </HomeStack.Navigator>
//   )
// }