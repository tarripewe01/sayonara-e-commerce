import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, ScrollView, StatusBar, TouchableOpacity, FlatList } from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import COLOR from '../../utils/Tools';
import NotifCard from '../../components/Notif Card';

import { useSelector, useDispatch } from 'react-redux';
import { NotificationSellerAction } from '../../../store/action/NotificationSellerAction';

const NotifScreen = ({navigation}) => {
  const userProfile = useSelector(state => state.userProfileReducer.user)
  const data = useSelector(state => state.notificationSellerReducer.data)
  const dispatch = useDispatch()
  const [dataFiltered,setDataFiltered] = useState([])

  useEffect(() => {
    dispatch(NotificationSellerAction())
    const filtering = data?.filter(x => x?.productId[0]?.seller?.username === userProfile?.username)
    setDataFiltered(filtering)
  }, [])

  return (
    <View >
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      <View style={{backgroundColor: COLOR.pureWhite, marginTop: 50, height: 50, flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <FontAwesome name='arrow-left' size={25} color={COLOR.green}
            style={{marginLeft: 15, marginTop: 10}}
          />
        </TouchableOpacity>
          <Text style={{fontSize: 20, marginLeft: 20, marginTop: 10}}>Notification</Text>
      </View>

    <ScrollView>
      <FlatList 
        data={dataFiltered}
        renderItem={({item}) => {
          return (
            <NotifCard 
                id={item._id}
                name={item.productId[0].name}
                image={item.productId[0].image}
                price={item.productId[0].price.$numberDecimal}
                status={item.status}
                buyer={item.user.username}
            />
          )
        }}
        keyExtractor={item => item.id}
      />
      </ScrollView>
    </View>
  )
}

export default NotifScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
