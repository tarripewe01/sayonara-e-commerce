import React, { useEffect } from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, Button, Image,} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ActivityIndicator, Avatar, Caption, Title } from 'react-native-paper'
import COLOR from '../../utils/Tools';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { TextInput } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';

import Header from '../../components/Header';
import Form from '../../components/Form';
import { getDetailProductSellerAction } from '../../../store/action/getDeatilProductSelerAction';

const EditProductScreen = ({navigation,route}) => {
  const data = useSelector(state => state.getDetailProductSellerReducer)
  const dispatch = useDispatch()
  const { idproduct } = route.params

  useEffect(()=>{
    dispatch(getDetailProductSellerAction(idproduct))
  },[])

  return (
    <View style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      <View style={{backgroundColor: COLOR.pureWhite, marginTop: 50, height: 50, flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <FontAwesome name='arrow-left' size={25} color={COLOR.green}
            style={{marginLeft: 15, marginTop: 10}}
          />
        </TouchableOpacity>
          <Text style={{fontSize: 20, marginLeft: 20, marginTop: 10}}>Edit Product</Text>
      </View>

      <ScrollView>
      <View style={{backgroundColor: COLOR.pureWhite, height: 750, marginTop: 15,}}>
        {/* <View >
          <View style={{backgroundColor: COLOR.yellow, height: 130, width: 130, borderRadius: 10,  marginTop: 15, marginLeft: 15, flexDirection: 'row'}}>
            <Image 
              source={{uri: 'https://www.permataindonesia.ac.id/wp-content/uploads/2015/11/LD0zWQz.jpg'}}
              style={{height: 130, width: 130}}
            />
            <TouchableOpacity
              onPress={()=>{}}
              style={{marginLeft: 100, width: 130, height: 40, marginTop: 50, borderWidth: 1, borderColor: COLOR.green, borderRadius: 5, backgroundColor: COLOR.green, justifyContent: 'center'}}
            >
              <Text style={{ alignItems: 'center', justifyContent: 'center', alignSelf: 'center', color: COLOR.pureWhite, fontSize: 16}}>Change Images</Text>
            </TouchableOpacity>
          </View>
        </View> */}
        {
          data?.loading ? (
            <View style={{flex: 1, justifyContent: 'center',alignItems: 'center', marginVertical: '70%'}}>
              <ActivityIndicator size="large" color={COLOR.darkGreen} />
            </View>
          ):(
            <Form data={{type: 'update',data: data?.data}} />
          )
        }
      </View>
      </ScrollView>
    </View>
  );
};

export default EditProductScreen;

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey)
  },
})

