import React, {useState} from 'react'
import { 
  StyleSheet, Text, 
  View, StatusBar, 
  TouchableOpacity 
} from 'react-native'

import axios from '../../helper/axios';

import * as Animatable from 'react-native-animatable';
import { TextInput } from 'react-native-gesture-handler';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FormData from 'form-data';

import COLOR from '../../utils/Tools/index';

const Register = ({navigation}) => {

  const [username,setUsername] = useState('')
  const [email,setEmail] = useState('')
  const [password,setPassword] = useState('')
  const [passwordConfirm,setPasswordConfirm] = useState('')

  const [data, setData] = useState ({
    
    username: '',
    email: '',
    password: '',
    confirm_password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    confirm_secureTextEntry: true
  });

  const textUserInputChange = (val) => {
    if( val.length !== 0) {
      setData({
        ...data,
        username: val,
        check_textUserInputChange: true
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textUserInputChange: false
      });
    }
  };

  const textInputChange = (val) => {
    if( val.length !== 0) {
      setData({
        ...data,
        email: val,
        check_textInputChange: true
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textInputChange: false
      });
    }
  };

  const handlePasswordChange = (val) => {
    setData({
      ...data,
      password: val
    });
  };

  const handleConfirmPasswordChange = (val) => {
    setData({
      ...data,
      confirm_password: val
    });
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry
    });
  };

  const updateConfirmSecureTextEntry = () => {
    setData({
      ...data,
      confirm_secureTextEntry: !data.confirm_secureTextEntry
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar 
        backgroundColor={(COLOR.white)} 
        barStyle='light-content'
      />
      <View>
      <Animatable.Image
        animation="bounceIn"
        source={require('../../assets/Images/logoFix.png')}
        style={styles.logo}
      />
      </View>
      <View style={styles.containerRegister}>
      <View style={styles.register}>
        <View style={styles.action}>
        <FontAwesome 
            name="user"
            color= {COLOR.green}
            size={22}
            style={{ marginTop: 10 }}
          />
          <TextInput 
            placeholder="Username"
            style={{marginLeft: 5}}
            autoCapitalize='none'
            value={username}
            onChangeText={(text) => {setUsername(text)}}
          />
          </View>
          <View style={styles.action}>
          <FontAwesome 
            name="envelope"
            color= {COLOR.green}
            size={20}
            style={{ marginTop: 10 }}
          />
          <TextInput 
            placeholder="Email"
            style={{marginLeft: 5}}
            autoCapitalize='none'
            value={email}
            onChangeText={(text) => {setEmail(text)}}
          />
          </View>
          <View style={styles.action}>
          <FontAwesome 
            name="lock"
            color= {COLOR.green}
            size={22}
            style={{ marginTop: 10 }}
          />
        <TextInput 
          placeholder="Password"
          secureTextEntry={data.secureTextEntry ? true : false}
          style={{marginLeft: 5}}
          value={password}
          onChangeText={(text) => {setPassword(text)}}
        />
        </View>
        <View style={styles.action}>
          <FontAwesome 
            name="lock"
            color= {COLOR.green}
            size={22}
            style={{ marginTop: 10 }}
          />
        <TextInput
          placeholder="Confirm Password"
          secureTextEntry={data.confirm_secureTextEntry ? true : false}
          style={{marginLeft: 5}}
          value={passwordConfirm}
          onChangeText={(text) => {setPasswordConfirm(text)}}
        />
        </View>
      </View>
      <TouchableOpacity
        onPress={() => {
          axios({
            method: 'POST',
            url: '/signup',
            data: {
              username: username,
              email: email,
              password: password,
              passwordConfirm: password
            }
          })
            .then (({data}) => {
              console.log(data)
              navigation.navigate('Home')
            })
            .catch(err => {console.log('ini error di Register', err.message)})
        }}
      >
        <Text
          style={styles.button1}>Register
        </Text>
      </TouchableOpacity>
      <View style={styles.register1}>
        <Text>Have an account?</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.text}>Login</Text>
        </TouchableOpacity>
      </View>
      
      </View>
    </View>
  )
}

export default Register

const styles = StyleSheet.create({
  container:{ 
    flex: 1,
    backgroundColor: COLOR.white
  },
  logo: {
    alignSelf: 'center',
    marginTop: 5,
    height: 150,
    width: 150
  },
  containerRegister: {
    backgroundColor: COLOR.white,
    width: 350,
    height: 450,
    alignSelf: 'center',
    borderRadius: 30,
  },
  register:{
    width: 300,
    height: 300,
    borderBottomColor: COLOR.green,
    paddingHorizontal: 20,
    marginLeft: 10,
    justifyContent: 'center'
  },
  action:{
    flexDirection: 'row',
    borderColor: COLOR.green,
    borderBottomWidth: 1,
    marginTop: 10
  },
  button1:{
    backgroundColor: COLOR.green,
    fontSize: 21,
    fontWeight: '700',
    color: COLOR.white,
    width: 200,
    height: 35,
    marginTop: 25,
    borderRadius: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  register1:{
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 50
  },
  text:{
    color: COLOR.green,
    marginLeft: 5
  },
})
