import React, { useEffect } from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, Button, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Avatar, Caption, Title } from 'react-native-paper'

import AsyncStorage from '@react-native-async-storage/async-storage';
import { BASE_URL, TOKEN } from '../../../store/constant/general';
import Feather from 'react-native-vector-icons/Feather';

import COLOR from '../../utils/Tools';
import ContentProfile from '../../components/Banner/Profile';
import { userProfileAction } from '../../../store/action/userProfileAction';
import { useDispatch, useSelector } from 'react-redux';

const ProfileScreen = ({navigation}) => {

  const userProfile = useSelector (state => state.userProfileReducer.user)
  const myProfile = useSelector (state => state.getProfileReducer)

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(userProfileAction())
    console.log('myprofile',myProfile.profile.data.image)
  }, [])

  return (
    <ScrollView style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      {
        myProfile?.loading ? (
          <View style={{backgroundColor: COLOR.green, paddingTop: 50, height: 200, borderBottomEndRadius: 50, justifyContent: 'center', alignItems: 'center', marginLeft: -5, borderBottomLeftRadius: 50}}>
            <Text style={{color: 'white'}}>Loading..</Text>
          </View>
        ) : (
          <View style={{backgroundColor: COLOR.green, paddingTop: 50, height: 200, borderBottomEndRadius: 50, justifyContent: 'center', alignItems: 'center', marginLeft: -5, borderBottomLeftRadius: 50}}>
          <View style={styles.profileBox}>
            <View style={styles.profileContent}>
              <Avatar.Image source={{uri:`${BASE_URL}${myProfile.profile.data.image}`}} size={70}/>
            <View style={{marginLeft: 20, marginTop: 10}}>
              <Caption style={styles.caption}>{myProfile.profile.data.fullName}</Caption>
              <Title style={styles.title}>@{myProfile.profile.data.username}</Title>
              <Title style={styles.title1}>{myProfile.profile.data.email}</Title>
            </View> 
            <TouchableOpacity onPress={()=> navigation.navigate('Edit Profile')}>
              <Feather 
                name="edit" 
                size={23} 
                style={styles.profileIcon}
              /> 
              </TouchableOpacity>
            </View>
          </View>
        </View>
        )
      }
      <ContentProfile />
      <View>
        <TouchableOpacity 
          onPress={()=> {
            AsyncStorage.removeItem('access_token')
            navigation.navigate('Login')}} 
            style={styles.logout}
        >
          <Text style={styles.textLogout}>LOGOUT</Text>
        </TouchableOpacity>
      </View>
       
    </ScrollView>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey)
  },
  profileBox:{
    backgroundColor: 
    COLOR.white, 
    width: 300, 
    alignSelf: 'center', 
    height: 100, 
    marginTop: 30, 
    borderRadius: 10
  },
  profileContent: {
    flexDirection: 'row',  
    marginTop: 15, 
    marginLeft: 10
  },
    caption: {
    fontSize: 20, 
    fontWeight: 'bold'
  },
  title: {
    fontSize: 14, 
    color:'#cccdcc', 
    opacity: 2,
    marginTop: -6
  },
  title1: {
    fontSize: 14, 
    color:'#cccdcc', 
    opacity: 2,
    marginTop: -10
  },
  profileIcon: {
    color: COLOR.green,
  },
  logout: {
    width: 350,
    height: 35,
    borderRadius: 10,
    backgroundColor: COLOR.green,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 170
  },
  textLogout: {
    fontSize: 20,
    fontWeight: 'bold',
    color: COLOR.white
  }
})

