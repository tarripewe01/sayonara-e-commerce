import React from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, Button, Image,} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Avatar, Caption, Title } from 'react-native-paper'
import COLOR from '../../utils/Tools';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { TextInput } from 'react-native-paper';

const Header = ({navigation}) => {
  return (
    <View style={{backgroundColor: COLOR.pureWhite, marginTop: 50, height: 50, flexDirection: 'row', justifyContent: 'space-between'}}>
        <TouchableOpacity onPress={() => navigation.navigate('Search')}>
          <FontAwesome name='arrow-left' size={25} color={COLOR.green}
            style={{marginLeft: 15, marginTop: 10}}
          />
        </TouchableOpacity>
          <View style={{flexDirection: 'row', marginRight: 15, marginTop: 10}}>
            <TouchableOpacity 
              style={{marginRight: 10, marginTop: 3}}
              onPress={() => navigation.navigate('Cart')}
              >
              <FontAwesome 
                name='shopping-basket'
                color={COLOR.green}
                size={23}
              />
            </TouchableOpacity>
            <TouchableOpacity 
              style={{marginLeft: 10}}
              onPress={() => navigation.navigate('Home')}
              >
              <FontAwesome 
                name='home'
                color={COLOR.green}
                size={30}
              />
            </TouchableOpacity>
          </View>
      </View>
  )
}

export default Header

const styles = StyleSheet.create({})
